import 'reflect-metadata'; 
require('zone.js');

import { platformBrowserDynamic } from '@angular/platform-browser-dynamic'; 
import { AppModule } from './lib/AppModule';

import { Version } from '@microsoft/sp-core-library';
import {
  BaseClientSideWebPart,
  IPropertyPaneConfiguration,
  PropertyPaneTextField
} from '@microsoft/sp-webpart-base';

import { escape } from '@microsoft/sp-lodash-subset';
import styles from './AppWebPart.module.scss';
import * as strings from 'AppWebPartStrings';
import { IAppWebPartProps } from './IAppWebPartProps';

export default class AppWebPartWebPart extends BaseClientSideWebPart<IAppWebPartProps> {

  public render(): void {
    // this.domElement.innerHTML = `
    //   <div class="${styles.app}">
    //     <div class="${styles.container}">
    //       <div class="ms-Grid-row ms-bgColor-themeDark ms-fontColor-white ${styles.row}">
    //         <div class="ms-Grid-col ms-lg10 ms-xl8 ms-xlPush2 ms-lgPush1">
    //           <span class="ms-font-xl ms-fontColor-white">Welcome to SharePoint!</span>
    //           <p class="ms-font-l ms-fontColor-white">Customize SharePoint experiences using Web Parts.</p>
    //           <p class="ms-font-l ms-fontColor-white">${escape(this.properties.description)}</p>
    //           <a href="https://aka.ms/spfx" class="${styles.button}">
    //             <span class="${styles.label}">Learn more</span>
    //           </a>
    //         </div>
    //       </div>
    //     </div>
    //   </div>`;
    this.domElement.innerHTML =  `<my-app></my-app>`; 
    platformBrowserDynamic().bootstrapModule(AppModule);    
  }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('description', {
                  label: strings.DescriptionFieldLabel
                })
              ]
            }
          ]
        }
      ]
    };
  }
}
